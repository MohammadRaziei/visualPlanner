/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionPlayOn;
    QAction *actionPlayOff;
    QWidget *centralWidget;
    QLabel *field;
    QTabWidget *tabWidget;
    QWidget *tab;
    QSpinBox *spinBox;
    QPushButton *browseBtn;
    QPushButton *addBtn;
    QPushButton *resetBtn;
    QPushButton *saveBtn;
    QPushButton *removeBtn;
    QLabel *ballLabel;
    QTabWidget *POtabWidget;
    QWidget *tab_7;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QComboBox *comboBox_2;
    QPushButton *POBtn;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *POTBPosX;
    QLineEdit *POTBPosY;
    QLabel *label_3;
    QLineEdit *POTBPosAng;
    QLabel *label_4;
    QLineEdit *POTBPosTol;
    QPushButton *newBtn;
    QPushButton *tagsBtn;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuMode;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1280, 740);
        MainWindow->setMinimumSize(QSize(1280, 740));
        MainWindow->setMaximumSize(QSize(1280, 740));
        MainWindow->setBaseSize(QSize(1280, 740));
        actionPlayOn = new QAction(MainWindow);
        actionPlayOn->setObjectName(QStringLiteral("actionPlayOn"));
        actionPlayOn->setCheckable(true);
        actionPlayOn->setPriority(QAction::NormalPriority);
        actionPlayOff = new QAction(MainWindow);
        actionPlayOff->setObjectName(QStringLiteral("actionPlayOff"));
        actionPlayOff->setCheckable(true);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        field = new QLabel(centralWidget);
        field->setObjectName(QStringLiteral("field"));
        field->setGeometry(QRect(10, 10, 351, 655));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(880, 10, 391, 591));
        tabWidget->setFocusPolicy(Qt::NoFocus);
        tabWidget->setLayoutDirection(Qt::LeftToRight);
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        tabWidget->addTab(tab, QString());
        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(140, 620, 48, 27));
        spinBox->setFocusPolicy(Qt::NoFocus);
        spinBox->setMinimum(0);
        spinBox->setMaximum(999);
        spinBox->setValue(0);
        browseBtn = new QPushButton(centralWidget);
        browseBtn->setObjectName(QStringLiteral("browseBtn"));
        browseBtn->setGeometry(QRect(20, 620, 99, 27));
        addBtn = new QPushButton(centralWidget);
        addBtn->setObjectName(QStringLiteral("addBtn"));
        addBtn->setGeometry(QRect(200, 620, 99, 27));
        resetBtn = new QPushButton(centralWidget);
        resetBtn->setObjectName(QStringLiteral("resetBtn"));
        resetBtn->setGeometry(QRect(310, 620, 99, 27));
        saveBtn = new QPushButton(centralWidget);
        saveBtn->setObjectName(QStringLiteral("saveBtn"));
        saveBtn->setGeometry(QRect(420, 620, 99, 27));
        removeBtn = new QPushButton(centralWidget);
        removeBtn->setObjectName(QStringLiteral("removeBtn"));
        removeBtn->setGeometry(QRect(530, 620, 99, 27));
        ballLabel = new QLabel(centralWidget);
        ballLabel->setObjectName(QStringLiteral("ballLabel"));
        ballLabel->setGeometry(QRect(1160, 616, 67, 41));
        POtabWidget = new QTabWidget(centralWidget);
        POtabWidget->setObjectName(QStringLiteral("POtabWidget"));
        POtabWidget->setGeometry(QRect(480, 10, 391, 591));
        POtabWidget->setFocusPolicy(Qt::NoFocus);
        tab_7 = new QWidget();
        tab_7->setObjectName(QStringLiteral("tab_7"));
        layoutWidget = new QWidget(tab_7);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 361, 29));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        comboBox_2 = new QComboBox(layoutWidget);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));

        horizontalLayout->addWidget(comboBox_2);

        POBtn = new QPushButton(layoutWidget);
        POBtn->setObjectName(QStringLiteral("POBtn"));

        horizontalLayout->addWidget(POBtn);

        layoutWidget1 = new QWidget(tab_7);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 170, 379, 41));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget1);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_2->addWidget(label);

        POTBPosX = new QLineEdit(layoutWidget1);
        POTBPosX->setObjectName(QStringLiteral("POTBPosX"));

        horizontalLayout_2->addWidget(POTBPosX);

        POTBPosY = new QLineEdit(layoutWidget1);
        POTBPosY->setObjectName(QStringLiteral("POTBPosY"));

        horizontalLayout_2->addWidget(POTBPosY);

        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_2->addWidget(label_3);

        POTBPosAng = new QLineEdit(layoutWidget1);
        POTBPosAng->setObjectName(QStringLiteral("POTBPosAng"));

        horizontalLayout_2->addWidget(POTBPosAng);

        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_2->addWidget(label_4);

        POTBPosTol = new QLineEdit(layoutWidget1);
        POTBPosTol->setObjectName(QStringLiteral("POTBPosTol"));

        horizontalLayout_2->addWidget(POTBPosTol);

        POtabWidget->addTab(tab_7, QString());
        newBtn = new QPushButton(centralWidget);
        newBtn->setObjectName(QStringLiteral("newBtn"));
        newBtn->setGeometry(QRect(640, 620, 99, 27));
        tagsBtn = new QPushButton(centralWidget);
        tagsBtn->setObjectName(QStringLiteral("tagsBtn"));
        tagsBtn->setGeometry(QRect(780, 620, 99, 27));
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1280, 25));
        menuBar->setLayoutDirection(Qt::RightToLeft);
        menuBar->setNativeMenuBar(true);
        menuMode = new QMenu(menuBar);
        menuMode->setObjectName(QStringLiteral("menuMode"));
        menuMode->setContextMenuPolicy(Qt::DefaultContextMenu);
        menuMode->setLayoutDirection(Qt::RightToLeft);
        menuMode->setTearOffEnabled(false);
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuMode->menuAction());
        menuMode->addAction(actionPlayOn);
        menuMode->addAction(actionPlayOff);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);
        POtabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionPlayOn->setText(QApplication::translate("MainWindow", "PlayOn", 0));
        actionPlayOn->setShortcut(QApplication::translate("MainWindow", "Ctrl+N", 0));
        actionPlayOff->setText(QApplication::translate("MainWindow", "PlayOff", 0));
        actionPlayOff->setShortcut(QApplication::translate("MainWindow", "Ctrl+M", 0));
        field->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Ball/Settings", 0));
        browseBtn->setText(QApplication::translate("MainWindow", "Browse", 0));
        addBtn->setText(QApplication::translate("MainWindow", "Apply", 0));
        resetBtn->setText(QApplication::translate("MainWindow", "Reset", 0));
        saveBtn->setText(QApplication::translate("MainWindow", "Save", 0));
        removeBtn->setText(QApplication::translate("MainWindow", "Remove", 0));
        ballLabel->setText(QString());
        label_2->setText(QApplication::translate("MainWindow", "Agent Szie", 0));
        comboBox_2->clear();
        comboBox_2->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "1 Agent", 0)
         << QApplication::translate("MainWindow", "2 Agent", 0)
         << QApplication::translate("MainWindow", "3 Agent", 0)
         << QApplication::translate("MainWindow", "4 Agent", 0)
         << QApplication::translate("MainWindow", "5 Agent", 0)
         << QApplication::translate("MainWindow", "6 Agent", 0)
        );
        POBtn->setText(QApplication::translate("MainWindow", "Show All", 0));
        label->setText(QApplication::translate("MainWindow", "Position", 0));
        label_3->setText(QApplication::translate("MainWindow", "Ang", 0));
        POTBPosAng->setText(QApplication::translate("MainWindow", "0", 0));
        label_4->setText(QApplication::translate("MainWindow", "Tol", 0));
        POTBPosTol->setText(QApplication::translate("MainWindow", "0", 0));
        POtabWidget->setTabText(POtabWidget->indexOf(tab_7), QApplication::translate("MainWindow", "PlayOff", 0));
        newBtn->setText(QApplication::translate("MainWindow", "New", 0));
        tagsBtn->setText(QApplication::translate("MainWindow", "Tags", 0));
        menuMode->setTitle(QApplication::translate("MainWindow", "Mode", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
