/********************************************************************************
** Form generated from reading UI file 'tags.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TAGS_H
#define UI_TAGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_tags
{
public:
    QLineEdit *textTag;
    QPushButton *addTagBtn;
    QLabel *label;

    void setupUi(QDialog *tags)
    {
        if (tags->objectName().isEmpty())
            tags->setObjectName(QStringLiteral("tags"));
        tags->resize(500, 300);
        textTag = new QLineEdit(tags);
        textTag->setObjectName(QStringLiteral("textTag"));
        textTag->setGeometry(QRect(10, 10, 261, 51));
        QFont font;
        font.setFamily(QStringLiteral("Ubuntu Mono"));
        font.setPointSize(13);
        textTag->setFont(font);
        addTagBtn = new QPushButton(tags);
        addTagBtn->setObjectName(QStringLiteral("addTagBtn"));
        addTagBtn->setGeometry(QRect(280, 10, 211, 51));
        label = new QLabel(tags);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 70, 67, 17));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        label->setFont(font1);

        retranslateUi(tags);

        QMetaObject::connectSlotsByName(tags);
    } // setupUi

    void retranslateUi(QDialog *tags)
    {
        tags->setWindowTitle(QApplication::translate("tags", "Tags", 0));
        addTagBtn->setText(QApplication::translate("tags", "Add Tag", 0));
        label->setText(QApplication::translate("tags", "Tags", 0));
    } // retranslateUi

};

namespace Ui {
    class tags: public Ui_tags {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TAGS_H
